# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
- [Author](#author)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### Screenshot

![preview](./screenshot.png)

### Links

- Solution URL: [Gitlab](https://gitlab.com/chinosan/qr-code-fem)
- Live Site URL: [Vercel](https://your-live-site-url.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow



## Author

- Website - [devDonaldo](https://portfolio-six-sage-13.vercel.app/)
- Frontend Mentor - [@devLuisDonaldo](https://www.frontendmentor.io/profile/devLuisDonaldo)
- Linkedin - [@yourusername](https://www.linkedin.com/in/campos-luis-donaldo/)


